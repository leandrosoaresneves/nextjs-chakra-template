export interface Package {
    name: string;
    kg: number;
    listPrice: number;
    discountPrice: number;
    quantityDiscount: number;
  }

  export interface Taxes {
    name: string;
    label: string;
    value: number;
  }
  export interface Product {
    name: string;
    packages: Package[];
    taxes: Taxes[];
    packageSelected: Package;
  }

  export interface Item {
    product: Product;
    price: string;
    priceWithTaxes: string;
    shipping: string;
    quantity: number;
    total: string;
  }

  export interface Quotation {
    id: string;
    items: Item[];
    totalUsd: number;
    totalBrl: number;
  }

  export interface Shipping {
    name: string;
  }