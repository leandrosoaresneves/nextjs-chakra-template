import { Product, Package, Taxes, Item, Quotation, Shipping } from "../models";

const productsList: Product[] = [
  {
    name: "NP 95 BR",
    packages: [
      {
        name: "IBC",
        kg: 1000,
        listPrice: 5,
        discountPrice: 4.51,
        quantityDiscount: 10000,
      },
      {
        name: "TAMBOR",
        kg: 215,
        listPrice: 5.5,
        discountPrice: 4.96,
        quantityDiscount: 5000,
      },
      {
        name: "BOMBONA",
        kg: 50,
        listPrice: 6,
        discountPrice: 5.42,
        quantityDiscount: 1000,
      },
    ],
    taxes: [
      {
        name: "pis_cofins",
        label: "9,25%",
        value: 0.0925,
      },
      {
        name: "icms",
        label: "12%",
        value: 0.12,
      },
      {
        name: "ipi",
        label: "5%",
        value: 0.05,
      },
    ],
    packageSelected: {} as Package,
  },
];

const shippingItems: Shipping[] = [
  {
    name: "CIF - Despacho",
  },
  {
    name: "CIF - Redespacho",
  },
  {
    name: "FOB - Retira",
  },
];

export async function getAllProducts(): Promise<Product[]> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(productsList), 1000);
  });
}

export async function getPackagesFromProduct(
  productName: string
): Promise<Package[]> {
  return new Promise((resolve) => {
    const packages = productsList.find(
      (product) => product.name === productName
    ).packages;
    setTimeout(() => resolve(packages), 1000);
  });
}

export async function getShippings(): Promise<Shipping[]> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(shippingItems), 1000);
  });
}

export async function getProductByName(name: string): Promise<Product> {
  return new Promise((resolve) => {
    setTimeout(() => {
      const product = productsList.find((p) => p.name === name);
      resolve(product);
    }, 1000);
  });
}

export function getPackageBy(name: string): Package | undefined {
  let packaging: Package | undefined;
  productsList.forEach((product) => {
    packaging = product.packages.find(
      (packageItem) => packageItem.name === name
    );
  });

  return packaging;
}

export function getTotalPriceBy(
  packageName: string,
  quantity: number
): number | undefined {
  const packaging = getPackageBy(packageName);

  if (!packaging) return undefined;

  const taxes = getTaxesBy(packageName);

  const { kg } = packaging;
  const unitPriceWithTaxes = applyTaxes(
    getUnitPriceBy(packaging, quantity),
    taxes
  );

  return kg * quantity * unitPriceWithTaxes;
}

function getTaxesBy(packageName: string): Taxes[] {
  return productsList.find((product) =>
    product.packages.find(
      (productPackage) => productPackage.name === packageName
    )
  ).taxes;
}

export function getItemBy(
  product: Product,
  packageName: string,
  quantity: number,
  shipping: string
): Item | undefined {
  const packaging = getPackageBy(packageName);

  if (!packaging) return undefined;

  product.packageSelected = packaging;
  const unitPrice = getUnitPriceBy(packaging, quantity);

  return {
    product,
    price: unitPrice.toFixed(2),
    priceWithTaxes: applyTaxes(unitPrice, product.taxes).toFixed(2),
    shipping,
    quantity,
    total: getTotalPriceBy(packageName, quantity).toFixed(2),
  };
}

const applyTaxes = (price: number, taxes: Taxes[]) =>
  taxes.reduce((acc, tax) => acc * (1 + tax.value), price);

function getUnitPriceBy(packaging: Package, quantity: number): number {
  const { listPrice, discountPrice, quantityDiscount } = packaging;
  return quantity > quantityDiscount ? discountPrice : listPrice;
}

export function getQuotationByItems(
  items: Item[],
  usdBrlQuotation: number
): Quotation {
  const total = getQuotationTotal(items);
  return {
    id: "1",
    items,
    totalUsd: total,
    totalBrl: total * usdBrlQuotation,
  };
}

function getQuotationTotal(items: Item[]): number {
  return items.reduce((acc, item) => acc + parseFloat(item.total), 0);
}
