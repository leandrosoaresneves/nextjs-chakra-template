import { Avatar, Box, Flex, Text } from "@chakra-ui/react";

interface ProfileProps {
  showProfileData?: boolean;
}

export function Profile({ showProfileData = true }: ProfileProps) {
  return (
    <Flex align="center">
      {showProfileData && (
        <Box mr="4" textAlign="right">
          <Text>Leandro Neves</Text>
          <Text color="gray.300" fontSize="small">
            leandrosoaresneves@gmail.com
          </Text>
        </Box>
      )}

      <Avatar
        size="md"
        name="Leandro Neves"
        src="https://github.com/leandrosoares6.png"
      />
    </Flex>
  );
}
