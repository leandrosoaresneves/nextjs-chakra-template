import { FormControl, FormLabel, Select as ChakraSelect, SelectProps as ChakraSelectProps } from '@chakra-ui/react'

interface SelectProps extends ChakraSelectProps {
    children: React.ReactNode;
    name: string;
    label?: string;
}

export function Select({ name, label, children, ...rest }: SelectProps) {
  return (
    <FormControl>
      { !!label && <FormLabel htmlFor={name}>{label}</FormLabel> }

      <ChakraSelect
        id={name}
        name={name}
        focusBorderColor="green.500"
        bgColor="gray.900"
        variant="filled"
        _hover={{
          bg: "gray.900",
        }}
        size="lg"
        {...rest}
      >{children}</ChakraSelect>
    </FormControl>
  );
}
