import {
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
  theme,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  Text,
  useBreakpointValue,
} from "@chakra-ui/react";
import Link from "next/link";
import { useEffect, useState } from "react";
import { Input } from "../components/Form/Input";
import { Select } from "../components/Form/Select";
import { Header } from "../components/Header";
import { Sidebar } from "../components/Sidebar";
import { Item, Package, Product, Quotation, Shipping } from "../models";

import Script from "next/script";
import {
  getAllProducts,
  getItemBy,
  getPackageBy,
  getPackagesFromProduct,
  getQuotationByItems,
  getShippings,
  getTotalPriceBy,
} from "../services/ProductService";

export default function QuotationPage() {
  const isWideVersion = useBreakpointValue({
    base: false,
    lg: true,
  });

  const displayType = isWideVersion ? "" : "none";

  const [products, setProducts] = useState([] as Product[]);
  const [packages, setPackages] = useState([] as Package[]);
  const [shippings, setShippings] = useState([] as Shipping[]);
  const [product, setProduct] = useState({} as Product);
  const [priceWithDiscount, setPriceWithDiscount] = useState(0);
  const [normalPrice, setNormalPrice] = useState(0);
  const [packageName, setPackageName] = useState("");
  const [productsSelected, setProductsSelected] = useState([] as Item[]);
  const [quotation, setQuotation] = useState({} as Quotation);
  const [usdBrlQuotation, setUsdBrlQuotation] = useState(4.6746);
  const [shipping, setShipping] = useState("");
  const [quantity, setQuantity] = useState(undefined);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    getAllProducts().then((productsFromApi) => {
      setProducts(productsFromApi);
    });
  }, []);

  useEffect(() => {
    getShippings().then((shippingsFromApi) => {
      setShippings(shippingsFromApi);
    });
  }, []);

  useEffect(() => {
    product.name &&
      getPackagesFromProduct(product.name).then((packagesFromApi) => {
        setPackages(packagesFromApi);
      });
  }, [product]);

  useEffect(() => {
    calculateTotal();
  }, [quantity, packageName]);

  useEffect(() => {
    handleUpdateQuotation();
  }, [productsSelected]);

  useEffect(() => {
    packageName.length &&
      product.packages &&
      product.packages.find((p) => {
        if (p.name === packageName) {
          setNormalPrice(p.listPrice);
          setPriceWithDiscount(p.discountPrice);
        }
      });
  }, [packageName]);

  const handleSelectProduct = (productName: string) => {
    setProduct(products.find((p) => p.name === productName));
  };

  const handleAddQuotationItem = () => {
    const packaging = getPackageBy(packageName);

    if (packaging) {
      const item = getItemBy(product, packageName, quantity, shipping);
      setProductsSelected((prevState) => [...prevState, item]);
    }
  };

  const handleUpdateQuotation = () => {
    setQuotation(getQuotationByItems(productsSelected, usdBrlQuotation));
  };

  const handleClear = () => {
    setProduct(undefined);
    setPackageName("");
    setQuantity(undefined);
    setTotal(0);
  };

  const calculateTotal = () => {
    setTotal(getTotalPriceBy(packageName, quantity) || 0);
  };

  const handleGeneratePdf = async () => {
    await window["createPdf"]();
  };

  return (
    <Box>
      <Script src="/api/js-pdf-template.js" strategy="afterInteractive" />
      <Script src="/api/pdfGenerator.js" strategy="afterInteractive" />
      <Script
        src="/api/usdQuotation.js"
        strategy="afterInteractive"
        onLoad={() => {
          // @ts-ignore
          setUsdBrlQuotation(Number(window.quotation(1)));
        }}
      />
      <Header />

      <Flex w="100%" my="6" maxWidth={1480} mx="auto" px="6">
        <Sidebar />

        <Box flex="1" borderRadius={8} bg="gray.800" p={["6", "8"]}>
          <Heading size="lg" fontWeight="normal">
            Nova Cotação
          </Heading>

          <Divider my="6" borderColor="gray.700" />

          <VStack spacing={["6", "8"]}>
            <SimpleGrid minChildWidth="240px" spacing={["6", "8"]} w="100%">
              <Select name="client" label="Cliente">
                <option
                  style={{ background: theme.colors.gray["900"] }}
                  value=""
                >
                  Selecione
                </option>
                <option
                  style={{ background: theme.colors.gray["900"] }}
                  value="SUN PRODUTOS QUÍMICOS LTDA"
                >
                  SUN PRODUTOS QUÍMICOS LTDA
                </option>
              </Select>

              <Select
                name="product"
                label="Produto"
                onChange={(e) => handleSelectProduct(e.target.value)}
              >
                <option
                  style={{ background: theme.colors.gray["900"] }}
                  value=""
                >
                  Selecione
                </option>
                {products.map((p) => (
                  <option
                    key={p.name}
                    value={p.name}
                    style={{ background: theme.colors.gray["900"] }}
                  >
                    {p.name}
                  </option>
                ))}
              </Select>

              <Select
                name="packaging"
                label="Embalagem"
                onChange={(e) => setPackageName(e.target.value)}
              >
                <option
                  style={{ background: theme.colors.gray["900"] }}
                  value=""
                >
                  Selecione
                </option>
                {packages.map((p) => (
                  <option
                    key={p.name}
                    value={p.name}
                    style={{ background: theme.colors.gray["900"] }}
                  >
                    {`${p.name} ${p.kg}kg`}
                  </option>
                ))}
              </Select>

              <Input
                name="quantity"
                type="number"
                label="Quantidade"
                value={quantity}
                onChange={(e) => setQuantity(Number(e.target.value))}
              />

              <Select
                name="shipping"
                label="Tipo de Frete"
                value={shipping}
                onChange={(e) => setShipping(e.target.value)}
              >
                <option
                  style={{ background: theme.colors.gray["900"] }}
                  value=""
                >
                  Selecione
                </option>
                {shippings.map((s) => (
                  <option
                    key={s.name}
                    value={s.name}
                    style={{ background: theme.colors.gray["900"] }}
                  >
                    {s.name}
                  </option>
                ))}
              </Select>
            </SimpleGrid>

            <SimpleGrid minChildWidth="240px" spacing={["6", "8"]} w="100%">
              <Input
                name="listValue"
                label="(USD/Kg - NET)"
                value={normalPrice}
                disabled
              />
              <Input
                name="discountPrice"
                label="Preço. Mín. (USD/Kg - NET)"
                value={priceWithDiscount}
                disabled
              />
              <Input
                name="usdBrlQuotation"
                label="Cotação atual (PTAX)"
                value={usdBrlQuotation}
                disabled
              />
              <Input
                name="totalValue"
                label="Valor Total (com impostos)"
                value={total.toFixed(2)}
                disabled
              />
            </SimpleGrid>
          </VStack>

          <Flex mt="8" justify="flex-end">
            <HStack spacing="4">
              <Button as="a" colorScheme="orange" onClick={handleClear}>
                Limpar
              </Button>
              <Button colorScheme="green" onClick={handleAddQuotationItem}>
                Adicionar
              </Button>
            </HStack>
          </Flex>
          {productsSelected.length > 0 && (
            <>
              <Heading size="lg" fontWeight="normal">
                Itens da Cotação
              </Heading>
              <Divider my="6" borderColor="gray.700" />
              <Table colorScheme="whiteAlpha">
                <Thead>
                  <Tr px={["4", "4", "6"]} color="gray.300" width="8">
                    <Th display={displayType}>Item</Th>
                    <Th>Produto</Th>
                    <Th>Embalagem</Th>
                    <Th display={displayType}>Preço sem Impostos</Th>
                    <Th display="none">PIS + COFINS</Th>
                    <Th display="none">ICMS</Th>
                    <Th display="none">IPI</Th>
                    <Th display={displayType}>Preço com Impostos</Th>
                    <Th display={displayType}>Tipo de Frete</Th>
                    <Th>Quantidade</Th>
                    <Th>Valor Total</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {productsSelected.map((item: Item, index) => (
                    <Tr key={index} px={["4", "4", "6"]}>
                      <Td display={displayType}>{index + 1}</Td>
                      <Td>{item.product.name}</Td>
                      <Td>{item.product.packageSelected.name}</Td>
                      <Td display={displayType}>{item.price}</Td>
                      <Td display="none">
                        {
                          product.taxes.find((t) => t.name === "pis_cofins")
                            .label
                        }
                      </Td>
                      <Td display="none">
                        {product.taxes.find((t) => t.name === "icms").label}
                      </Td>
                      <Td display="none">
                        {product.taxes.find((t) => t.name === "ipi").label}
                      </Td>
                      <Td display={displayType}>{item.priceWithTaxes}</Td>
                      <Td display={displayType}>{item.shipping}</Td>
                      <Td>{item.quantity}</Td>
                      <Td>{item.total}</Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
              <Flex mt="8" justify="center">
                <VStack spacing="4">
                  <Box alignItems="center">
                    <Text fontSize="md">TOTAL USD</Text>
                    <Text id="totalUsd" fontSize="sm">
                      {`$ ${quotation.totalUsd.toFixed(2)}`}
                    </Text>
                  </Box>
                  <Box alignItems="center">
                    <Text fontSize="md">TOTAL R$</Text>
                    <Text id="totalBrl" fontSize="sm">
                      {`$ ${quotation.totalBrl.toFixed(2)}`}
                    </Text>
                  </Box>
                </VStack>
              </Flex>
              <Flex mt="8" justify="flex-end">
                <HStack spacing="4">
                  <Link href="/dashboard" passHref>
                    <Button as="a" colorScheme="whiteAlpha">
                      Cancelar
                    </Button>
                  </Link>
                  <Button colorScheme="green" onClick={handleGeneratePdf}>
                    Salvar
                  </Button>
                </HStack>
              </Flex>
            </>
          )}
        </Box>
      </Flex>
    </Box>
  );
}
