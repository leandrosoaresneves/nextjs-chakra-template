function quotation(diaAnterior) {
  var quotation = "4.6746";
  var d = new Date();
  var m = d.getMonth() + 1;
  var a = d.getFullYear();
  var dia = d.getDate() - diaAnterior;
  var dataFinal = m + "-" + dia + "-" + a;
  var url =
    "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)?@dataCotacao='" +
    dataFinal +
    "'&$format=json";

  let request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) {
      var resposta = JSON.parse(request.responseText);
      var valores = resposta.value[0];
      quotation = valores.cotacaoVenda;
      // confere(valores);
    }
  }; // fecha onLoad

  request.send();
  return quotation;
} // fecha diaAnterior

// função pra conferir se existe cotacao
// function confere(valores) {
//   if (!valores) {
//     document.getElementById("usdQuotation").innerHTML = "teset";
//     console.log("Não tem cotação " + quotationDay + " atras");
//     quotationDay++;
//     cotacao(quotationDay);
//   } else {
//     document.getElementById("usdQuotation").innerHTML = "R$ " + valores.cotacaoVenda;
//     console.log("Tem cotaçãoo " + quotationDay + " atras");
//   }
// }
