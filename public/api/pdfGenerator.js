async function createPdf() {
  var productsTable = document.getElementsByTagName("table")[0];
  var productsTableColumnsAmount = productsTable.rows[0].cells.length;
  var productsAmount = productsTable.rows.length - 1;
  var filename = "cotacao-2022-04-11.pdf";

  if (productsAmount == 0) {
    alert("Adicione produtos para gerar o PDF");
    return;
  }

  var productsArray = getProductsArray(
    productsTable,
    productsTableColumnsAmount
  );

  console.log("productsArray", productsArray);
  console.log("productsTableColumnsAmount", productsTableColumnsAmount);

  const totalUsd = document
    .querySelector("#totalUsd")
    .textContent.replace("$", "");
  
    const totalBrl = document
    .querySelector("#totalBrl")
    .textContent.replace("$", "");

  const propsObject = {
    outputType: "blob",
    returnJsPDFDocObject: true,
    orientationLandscape: true,
    compress: true,
    logo: {
      src: "icon.png",
      width: 53.33,
      height: 26.66,
      margin: {
        top: 0,
        left: 0,
      },
    },
    business: {
      name: "INTERBRASIL COMERCIAL EXPORTADORA S/A",
      address:
        "RUA ALFREDO WEISS, 155, BOEHMERWALD SÃO BENTO DO SUL - SC 41 99286.8825",
      phone: "+55 41 3156-8500",
      email: "atendimento@intercroma.com",
      website: "www.intercroma.com",
    },
    contact: {
      label: ":",
      name: "SUN PRODUTOS QUÍMICOS LTDA",
      address:
        "VICTOR RODRIGUES RESENDE, 90 INDUSTRIAL, UBERLANDIA - MG 34 3218.3700",
      phone: "17681727000184",
      email: "cliente@email.com",
      otherInfo: "www.clientewebsite.com",
    },
    invoice: {
      label: "Cotação #: ",
      num: 19,
      invDate: "Data de Emissão 01/01/2021 18:12",
      invGenDate: "Data de Validade: 02/02/2021 10:17",
      headerBorder: true,
      tableBodyBorder: true,
      header: [
        {
          title: "ITEM",
          style: {
            width: 20,
          },
        },
        {
          title: "PRODUTO",
          style: {
            width: 20,
          },
        },
        {
          title: "EMBALAGEM",
          style: {
            width: 25,
          },
        },
        {
          title: "PREÇO S/ IMP.",
          style: {
            width: 30,
          },
        },
        {
          title: "PIS + COFINS",
          style: {
            width: 25,
          },
        },
        {
          title: "ICMS",
          style: {
            width: 15,
          },
        },
        {
          title: "IPI",
          style: {
            width: 15,
          },
        },
        {
          title: "PREÇO C/ IMP.",
          style: {
            width: 30,
          },
        },
        {
          title: "TIPO DE FRETE",
          style: {
            width: 30,
          },
        },
        { title: "QUANTIDADE"},
        { title: "VALOR TOTAL USD" },
      ],
      table: productsArray,
      invTotalLabel: "Total:",
      invTotal: totalUsd,
      invCurrency: "USD",
      row1: {
        col1: "SubTotal:",
        col2: totalUsd,
        col3: "USD",
        style: {
          fontSize: 10,
        },
      },
      row2: {
        col1: "SubTotal R$:",
        col2: totalBrl,
        col3: "BRL",
        style: {
          fontSize: 10,
        },
      },
      invDescLabel: "",
      invDesc: "",
    },
    footer: {
      text: "",
    },
    pageEnable: true,
    pageLabel: "Página ",
  };

  var cabecalhoTermo = "Termos Gerais de Cotação e Venda INTERCROMA:";
  var textoTermo =
    "\n\n• A Cotação é válida apenas pelo prazo de “Validade” indicado." +
    "\n\n• As condições comerciais estão sujeitas à confirmação quando do recebimento do pedido de compra do Cliente." +
    "\n\n• O Cliente é o único responsável por verificar se a especificação técnica dos produtos atende às suas necessidades produtivas e checar as condições de qualidade e especificação antes de seu uso em produção." +
    "\n\n• Aprovando esta cotação e pedido o Cliente valida que as respectivas especificações técnicas apresentadas atendem sua necessidade." +
    "\n\n• Em caso de importação, fabricação e/ou especificação técnica customizada para atendimento exclusivo do Cliente, este ficará obrigado a pagar pela integralidade do volume de produto dedicado, não sendo possível negociar o cancelamento do pedido." +
    "\n\n• Pelo caráter de distribuição, a única garantia dada é a de que os produtos serão entregues conforme as especificações técnicas indicadas pelo fabricante e/ou acordadas com a INTERCROMA. Nenhuma outra garantia (de performance, de comerciabilidade, de não infração, de adequação a uma finalidade específica, etc.) é concedida." +
    "\n\n• O Cliente deve conferir os produtos no momento da entrega e levantar qualquer reclamação imediatamente, sob pena de preclusão. E, em caso de recusa da entrega/nota fiscal, o Cliente deverá comunicar imediatamente seu contato comercial." +
    "\n\n• A INTERCROMA poderá, a seu critério exclusivo, substituir o produto, conceder crédito ao cliente no valor do preço pago ou devolver o valor nos casos em que o produto não atenda a especificação técnica acordada; porém toda e qualquer decisão tomada relativa a questões técnicas e de qualidade só serão deliberadas após investigação realizada pela INTERCROMA com apoio do fabricante do produto." +
    "\n\n• A propriedade, responsabilidade e o risco do produto (manuseio, uso, armazenamento, etc.) passarão ao Cliente no momento da entrega no local indicado e acordado entre as Partes, seja direto no depósito do Cliente ou na transportadora de redespacho indicada." +
    "\n\n• Nos casos de transporte redespacho o Cliente valida que os produtos aqui adquiridos sejam entregues e depositados na transportadora de redespacho indicada ao longo dessa cotação. Responsabilizando-se pela comunicação com o terceiro e isentando a INTERCROMA de qualquer custo relacionado a alterações a posteriori de local de entrega." +
    "\n\n• Caso o transporte seja responsabilidade do Cliente, também o será por qualquer fato daí decorrente, como acidentes, remediação ambiental, composição de carga, redespacho, autuações, etc. Ficando também responsável por indicar os dados da transportadora e motorista que farão a coleta no depósito da INTERCROMA." +
    "\n\n• O Cliente deverá apresentar à INTERCROMA, como condição de fornecimento, todas as licenças, autorizações e certificados atualizados exigidos pela legislação aplicável e necessários para a aquisição dos Produtos, especialmente em se tratando de produtos controlados e/ou perigosos. Inclusive referente a transportadora contratada" +
    "\n\n• O Cliente é responsável pelo descarte ambientalmente adequado das embalagens adquiridas." +
    "\n\n• Em caso de atraso no pagamento incidirão correção monetária, juros de mora e multa de xx% sobre o valor total em atraso, bem como custas de cartório e honorários advocatícios em caso de cobrança extrajudicial. O protesto em cartório ocorrerá após x (xxx) dias de atraso." +
    "\n\n• As Partes não serão responsabilizadas, em nenhuma circunstância, por danos indiretos (consequenciais, incidentais, perdas e danos, lucros cessantes, etc.)." +
    "\n\n• Ao aceitar a cotação e pedido, o Cliente automaticamente aceita os termos acima.";

  var textObjects = {
    maxWidth: 270,
    align: "left",
  };

  var pdfCreated = jsPDFInvoiceTemplate.default(propsObject);
  pdfCreated.jsPDFDocObject.addPage();
  pdfCreated.jsPDFDocObject.text(cabecalhoTermo, 10, 20, textObjects);
  pdfCreated.jsPDFDocObject.text(textoTermo, 10, 30, textObjects);

  pdfCreated.jsPDFDocObject.save(filename);
}

function getProductsArray(table, chunk) {
  let tempArray = Array.from(table.rows)
    .reduce((acc, row) => {
      const columns = Array.from(row.children).map(
        (column) => column.textContent || ""
      );

      return acc.concat(columns);
    }, [])
    .chunk(chunk);
  tempArray.shift();
  return tempArray;
}

/**
 * Define the chunk method in the prototype of an array
 * that returns an array with arrays of the given size.
 *
 * @param chunkSize {Integer} Size of every group
 */
Object.defineProperty(Array.prototype, "chunk", {
  value: function (chunkSize) {
    var temporal = [];

    for (var i = 0; i < this.length; i += chunkSize) {
      temporal.push(this.slice(i, i + chunkSize));
    }

    return temporal;
  },
});
