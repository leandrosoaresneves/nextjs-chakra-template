const product = document.querySelector('#productId')
const package = document.querySelector('#packageId')
const quantity = document.querySelector('#quantity')
const listValue = document.querySelector('#listValue')
const discountPrice = document.querySelector('#discountPrice')
const totalProductValue = document.querySelector('#totalValue')
const productTableBody = document.querySelector('#items')
const addButton = document.querySelector('#addButton')
const totalUsd = document.querySelector('#totalUsd')
const totalBrl = document.querySelector('#totalBrl')
let totalValue = 0
let selectedProduct = []

const productsList =[{
  name: 'NP 95 BR',
  packages: {
    ibc: {
      kg: 1000,
      listPrice: 5,
      discountPrice: 4.51,
      quantityDiscount: 10000,
    },
    tambor: {
      kg: 215,
      listPrice: 5.5,
      discountPrice: 4.96,
      quantityDiscount: 5000,
    },
    bombona: {
      kg: 50,
      listPrice: 6,
      discountPrice: 5.42,
      quantityDiscount: 1000,
    }
  }
}]

const products = []

package.addEventListener('change', updateValues, false)
quantity.addEventListener('input', calculateValue, false)
product.addEventListener('change', changeProduct, false)

function updateValues() {
  switch (package.value) {
    case 'ibc':
      discountPrice.setAttribute('value', formatValue(selectedProduct[0].packages.ibc.discountPrice))
      listValue.setAttribute('value', formatValue(selectedProduct[0].packages.ibc.listPrice))
      break

    case 'bombona':
      discountPrice.setAttribute('value', formatValue(selectedProduct[0].packages.bombona.discountPrice))
      listValue.setAttribute('value', formatValue(selectedProduct[0].packages.bombona.listPrice))
      break

    case 'tambor':
      discountPrice.setAttribute('value', formatValue(selectedProduct[0].packages.tambor.discountPrice))
      listValue.setAttribute('value', formatValue(selectedProduct[0].packages.tambor.listPrice))
      break

    default:
      break
  }

  const amount = calculateDiscount()
  totalProductValue.setAttribute('value', formatValue(amount))
}

function changeProduct() {
  selectedProduct = productsList.filter(item => {
    return item.name === product.value
  })

  updateValues()
}

function formatValue(value) {
  const valueFormatted = Number(value).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD'
  })

  return valueFormatted
}

function formatValueBrl(value) {
  const valueFormatted = Number(value).toLocaleString('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  })

  return valueFormatted
}

function calculateDiscount() {
  let amount
  let amountKg = quantity.value * selectedProduct[0].packages[package.value].kg

  if (amountKg >= selectedProduct[0].packages[package.value].quantityDiscount) {
    amount = amountKg * selectedProduct[0].packages[package.value].discountPrice

  } else {
    amount = amountKg * selectedProduct[0].packages[package.value].listPrice
  }

  return amount
}

function calculateValue() {
  if (product.value === '' || package.value === '') {
    quantity.value = ''
    alert('Preencha todos os campos!')
  }

  const amount = calculateDiscount()

  totalProductValue.setAttribute('value', formatValue(amount))
}

function addProduct() {
  if (product.value === '' || package.value === '' || quantity.value === '') {
    alert('Preencha todos os campos!')
    return
  }

  totalValue += calculateDiscount()

  const newProduct = {
    name: product.value,
    package: package.selectedOptions[0].textContent,
    quantity: quantity.value,
    listPrice: listValue.value,
    discountPrice: discountPrice.value,
    amount: totalProductValue.value
  }

  products.push(newProduct)

  const tr = document.createElement('tr')

  tr.innerHTML = `
    <tr>
      <td>${products.length}</td>
      <td>${newProduct.name}</td>
      <td>${newProduct.package}</td>
      <td>${newProduct.quantity}</td>
      <td>${newProduct.listPrice}</td>
      <td>${newProduct.amount}</td>
    </tr>
  `

  productTableBody.appendChild(tr)
  totalUsd.innerHTML = formatValue(totalValue)
  totalBrl.innerHTML = formatValueBrl(totalValue*5.06)
}